package my.first.rest;
 
import javax.ws.rs.Path;

import javax.ws.rs.QueryParam;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;


@Path("hello") //set your service url path to <base_url>/hello
               // the <base_url> is based on your application name, the servlet and the URL pattern from the web.xml configuration file
public class Hello {
	
	
	//16/11/2000
	//Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(name);
	
 
  @GET
  public String filter(@QueryParam("rentaldate") String name,
		  @QueryParam("numberofnights") int numberofnights,
		  @QueryParam("numberofrooms") int numberofrooms) throws ParseException {
	  
	  List<Date> rental = new ArrayList<Date>();
	  List<Integer> non=new ArrayList<Integer>();
	  List<Integer> nor=new ArrayList<Integer>();
      List<Integer> id=new ArrayList<Integer>();
	  
	  rental.add(new SimpleDateFormat("dd/MM/yyyy").parse("02/12/2010"));
	  rental.add(new SimpleDateFormat("dd/MM/yyyy").parse("04/11/1900"));
	  rental.add(new SimpleDateFormat("dd/MM/yyyy").parse("04/12/2010"));
	  rental.add(new SimpleDateFormat("dd/MM/yyyy").parse("03/12/2019"));
	  
	  non.add(2);
	  non.add(10);
	  non.add(3);
	  non.add(15);
	  
	  nor.add(2);
	  nor.add(10);
	  nor.add(3);
	  nor.add(15);
	  
	  id.add(1);
	  id.add(2);
	  id.add(3);
	  id.add(4);
	  
	  String sortie="";
	  Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(name);
	  
      for (int i = 0; i < id.size(); i++) {
    	  if(non.get(i)>=numberofnights & nor.get(i)>=numberofrooms & rental.get(i).compareTo(date1)==0) {
    		  sortie+="id:"+id.get(i).toString()+" ";
    	  }
      }
	  
      if(sortie=="") {
      	return "Hotel not Found";
      }
      else {
    	  return sortie; 
      }
	  
          
      }
}