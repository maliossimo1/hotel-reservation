package SOAP;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
 

public class A {
		public String information(String rentaldate, String numberofnights, String numberofrooms) throws ParseException, java.text.ParseException{
			
			//Exemple d'url pour tester
			//http://localhost:8080/Hotel/services/A/information?rentaldate=16/11/1999&numberofnights=6&numberofrooms=5
			
			Form form = new Form();
		       form.add("rentaldate",rentaldate);
		       form.add("numberofnights", numberofnights);
		       form.add("numberofrooms", numberofrooms);

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource target = client.resource("http://localhost:8080/rest/hello");
		
		ClientResponse response = target.queryParam("rentaldate",rentaldate)
	            .queryParam("numberofnights", numberofnights)
	            .queryParam("numberofrooms", numberofrooms)
	            .get(ClientResponse.class);
	    
	    
			return response.getEntity(String.class);
			
}
}



   
 
  